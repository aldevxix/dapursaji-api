<?php 
	
	/**
	 * API Model DapurSaji
	 */
	class apiModel extends DapurSaji
	{
		
		public function insertRegister ( $params )
		{
			
			$insert = $this->db->prepare ("
				INSERT INTO `tb_user` (`nama_lengkap`, `email`, `password`, `no_telepon`, `jenis_kelamin`, `level`)
				VALUES (:nama_lengkap, :email, :password, :no_telepon, :jenis_kelamin, :level);
			");

			return $insert->execute ( $params );
		}

		public function getEmail ( $params )
		{

			$select = $this->db->prepare ("
				SELECT `email` FROM `tb_user` WHERE `email` = :email;
			");

			$select->execute ( $params );

			return $select->fetchAll(PDO::FETCH_ASSOC);

		}

		public function getLogin ( $params )
		{

			$select = $this->db->prepare ("
				SELECT * FROM `tb_user` WHERE `email` = :email && `password` = :password;
			");

			$select->execute ( $params );

			return $select->fetchAll(PDO::FETCH_ASSOC);

		}

		public function getMenu ( $params )
		{

			$select = $this->db->prepare ("
				SELECT * FROM tb_menu;
			");

			$select->execute ( $params );

			return $select->fetchAll(PDO::FETCH_ASSOC);

		}

		public function insertMenu ( $params )
		{
			
			$insert = $this->db->prepare ("
				INSERT INTO `tb_menu` (`id_menu`, `nama_menu`, `ukuran`, `komposisi`, `stok`, `harga`, `gambar`	)
				VALUES (NULL, :nama_menu, :ukuran, :komposisi, :stok, :harga, :gambar);
			");

			return $insert->execute ( $params );

		}

		public function deleteMenu ( $params )
        {

            $delete = $this->db->prepare ("
                DELETE FROM `tb_menu` WHERE `id_menu` = :id_menu;
            ");

            return $delete->execute ( $params );

        }

        public function updateMenu ( $params )
		{
			
			$update = $this->db->prepare ("
				UPDATE `tb_menu` SET `nama_menu` = :nama_menu, `ukuran` = :ukuran, `komposisi` = :komposisi, `stok` = :stok, `harga` = :harga, `gambar` = :gambar WHERE `id_menu` = :id_menu;
			");

			return $update->execute ( $params );
			
		}

		public function getHistory ( $params )
		{

			$select = $this->db->prepare ("
				SELECT t.id_transaksi, t.id_user, t.id_order, t.status, t.cek, o.id_menu, o.jumlah, o.total_harga, o.id_order, m.id_menu, m.nama_menu, m.komposisi, m.gambar FROM tb_transaksi t, tb_order o, tb_menu m WHERE t.id_user = :id_user AND o.id_menu = m.id_menu AND t.id_order = o.id_order;
			");

			$select->execute ( $params );

			return $select->fetchAll(PDO::FETCH_ASSOC);

		}


	}
?>