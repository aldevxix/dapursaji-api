<?php

	$this->post('/register', \Api::class.":register");
	$this->post ('/login', \Api::class.":login");
	$this->post ('/getMenu', \Api::class.":getMenu");
	$this->post ('/insertMenu', \Api::class.":insertMenu");
	$this->post ('/deleteMenu', \Api::class.":deleteMenu");
	$this->post ('/updateMenu', \Api::class.":updateMenu");
	$this->post ('/getRating', \Api::class.":getRating");
	$this->post ('/getHistory', \Api::class.":getHistory");

	/**
	 * API DapurSaji
	 */
	class Api extends DapurSaji
	{
		
		public function register ( $request, $response, $args )
		{
			$this->params = $request->getParsedBody();

			$this->initModel ( "api" );

			$check = $this->apiModel->getEmail ( array (
				":email" => $this->params["email"],
			));

			if ( $check ) {
				return $response->withJSON(array(
                    "status" => false,
                    "message" => 'Email Sudah Digunakan',
                ));
			}
			else {
				$data = $this->apiModel->insertRegister ( array (
					":nama_lengkap" => $this->params["nama_lengkap"],
					":email" => $this->params["email"],
					":password" => $this->params["password"],
					":no_telepon" => $this->params["no_telepon"],
					":jenis_kelamin" => $this->params["jenis_kelamin"],
					":level" => $this->params["level"],
				));

				if ( $data ) {
		            return $response->withJSON ( array (
		                "status"    => $data,
		                "message"   => "Berhasil Register",
		                "data"      => array (
		                    "nama_lengkap"  => $this->params["nama_lengkap"],
		                    "email"  => $this->params["email"],
		                    "password"  => $this->params["password"],
		                    "no_telepon"  => $this->params["no_telepon"],
		                    "jenis_kelamin"  => $this->params["jenis_kelamin"],
		                    "level"  => $this->params["level"],
		                )
		            ));
	        	}
			}
			
		}

		public function login ( $request, $response, $args )
		{
			$this->params = $request->getParsedBody();
         
        	$this->initModel ( "api" );

        	$data = $this->apiModel->getLogin (array (
	            ":email" => $this->params["email"],
	            ":password" => $this->params["password"],
	        ));

	        if( $data ){
	            return $response->withJSON ( array (
	                "status"    => true,
	                "message"   => "Berhasil Login",
	                "data"      => $data,
	            ));
	        }else{
	            return $response->withJSON ( array (
	                "status"    => false,
	                "message"   => "Email / Password Salah",
	            ));
	        }
		}

		public function getMenu ( $request, $response, $args )
		{
			$this->params = $request->getParsedBody();
         
        	$this->initModel ( "api" );

        	$data = $this->apiModel->getMenu (array (
	          ));

	        if( $data ){
	            return $response->withJSON ( array (
	                "status"    => true,
	                "message"   => "Berhasil Ambil Menu",
	                "data"      => $data,
	            ));
	        }else{
	            return $response->withJSON ( array (
	                "status"    => false,
	                "message"   => "Menu Kosong",
	            ));
	        }
		}

		public function insertMenu ( $request, $response, $args )
		{
			$gambar='';
            $uploadedfile = $_FILES['gambar']['tmp_name'];
            
            $filename = $_FILES['gambar']['name'];

            $extension = getExtension($filename);

            $extension = strtolower($extension);
            if(($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")){

                $json = array("message" => 'error',"code"=>500);
                return;
            }else{
                $size=filesize($uploadedfile);
                if($extension == "jpg" || $extension == "jpeg"){

                    $src = imagecreatefromjpeg($uploadedfile);

                }else if($extension == "png"){

                    $src = imagecreatefrompng($uploadedfile);

                }else{

                    $src = imagecreatefromgif($uploadedfile);

                }
                list($width,$height)=getimagesize($uploadedfile);
                $imratio = 1;
                if($width != 0 && $height != 0)
                    $imratio = $width/$height;
                $imgNameRand = basename(microtime(true).".".$extension);
                $tmp=imagecreatetruecolor($width,$height);
                imagecopyresampled($tmp,$src,0,0,0,0,$width,$height,$width,$height);
                $filename = "Images/". $imgNameRand;
                imagejpeg($tmp, $filename, 100);

                imagedestroy($src);
                imagedestroy($tmp);
            }

			$this->params = $request->getParsedBody();
         
        	$this->initModel ( "api" );

        	$data = $this->apiModel->insertMenu (array (
	            ":nama_menu" => $this->params["nama_menu"],
	            ":ukuran" => $this->params["ukuran"],
	            ":komposisi" => $this->params["komposisi"],
	            ":stok" => $this->params["stok"],
	            ":harga" => $this->params["harga"],
	            ":gambar" => $filename,
	        ));

	        if( $data ){
	            return $response->withJSON ( array (
	                "status"    => true,
	                "message"   => "Berhasil Tambah Menu",
	                "data"      => array (
	                    "nama_menu"  => $this->params["nama_menu"],
	                    "ukuran"  => $this->params["ukuran"],
	                    "komposisi"  => $this->params["komposisi"],
	                    "stok"  => $this->params["stok"],
	                    "harga"  => $this->params["harga"],
	                    "gambar" => $filename,
	                )
	            ));
	        }else{
	            return $response->withJSON ( array (
	                "status"    => false,
	                "message"   => "Gagal Tambah Menu",
	            ));
	        }
		}

		public function deleteMenu ( $request , $response , $args )
        {
            $this->params = $request->getParsedBody();

            $this->initModel( "api" );

            $delete = $this->apiModel->deleteMenu (array (
                ":id_menu" => $this->params["id_menu"],
            ));

            if($delete){
                return $response->withJSON (array (
                    "status" => true,
                    "message"   => "Berhasil Delete Menu",
                ));
                
            }else{
                return $response->withJSON (array (
                    "status" => true,
                    "message"   => "Gagal Delete Menu",
                ));
                
            }
        }

        public function updateMenu ( $request, $response, $args )
		{
			$gambar='';
            $uploadedfile = $_FILES['gambar']['tmp_name'];
            
            $filename = $_FILES['gambar']['name'];

            $extension = getExtension($filename);

            $extension = strtolower($extension);
            if(($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")){

                $json = array("message" => 'error',"code"=>500);
                return;
            }else{
                $size=filesize($uploadedfile);
                if($extension == "jpg" || $extension == "jpeg"){

                    $src = imagecreatefromjpeg($uploadedfile);

                }else if($extension == "png"){

                    $src = imagecreatefrompng($uploadedfile);

                }else{

                    $src = imagecreatefromgif($uploadedfile);

                }
                list($width,$height)=getimagesize($uploadedfile);
                $imratio = 1;
                if($width != 0 && $height != 0)
                    $imratio = $width/$height;
                $imgNameRand = basename(microtime(true).".".$extension);
                $tmp=imagecreatetruecolor($width,$height);
                imagecopyresampled($tmp,$src,0,0,0,0,$width,$height,$width,$height);
                $filename = "Images/". $imgNameRand;
                imagejpeg($tmp, $filename, 100);

                imagedestroy($src);
                imagedestroy($tmp);
            }

			$this->params = $request->getParsedBody();
         
        	$this->initModel ( "api" );

        	$data = $this->apiModel->updateMenu (array (
        		":id_menu" => $this->params["id_menu"],
	            ":nama_menu" => $this->params["nama_menu"],
	            ":ukuran" => $this->params["ukuran"],
	            ":komposisi" => $this->params["komposisi"],
	            ":stok" => $this->params["stok"],
	            ":harga" => $this->params["harga"],
	            ":gambar" => $filename,
	        ));

	        if( $data ){
	            return $response->withJSON ( array (
	                "status"    => true,
	                "message"   => "Berhasil Edit Menu",
	                "data"      => array (
	                	"id_menu"  => $this->params["id_menu"],
	                    "nama_menu"  => $this->params["nama_menu"],
	                    "ukuran"  => $this->params["ukuran"],
	                    "komposisi"  => $this->params["komposisi"],
	                    "stok"  => $this->params["stok"],
	                    "harga"  => $this->params["harga"],
	                    "gambar" => $filename,
	                )
	            ));
	        }else{
	            return $response->withJSON ( array (
	                "status"    => false,
	                "message"   => "Gagal Edit Menu",
	            ));
	        }
		}

		public function getHistory ( $request, $response, $args )
		{
			$this->params = $request->getParsedBody();
         
        	$this->initModel ( "api" );

        	$data = $this->apiModel->getHistory (array (
	            ":id_user" => $this->params["id_user"],
	        ));

	        if( $data ){
	            return $response->withJSON ( array (
	                "status"    => true,
	                "message"   => "Berhasil Ambil History",
	                "data"      => $data,
	            ));
	        }else{
	            return $response->withJSON ( array (
	                "status"    => false,
	                "message"   => "Gagal Ambil History",
	            ));
	        }
		}


	}
	
	function getExtension($fname){
        $pos = strrpos($fname,".");
        if (!$pos) {return "";}
        $end = strlen($fname) - $pos;
        $ext = substr($fname,$pos+1,$end);
        return $ext;
    }

?>